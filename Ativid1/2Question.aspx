﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="2Question.aspx.cs" Inherits="Ativid1._2Question" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seleção de Dias da Semana</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #fff;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        #form1 {
            max-width: 400px;
            padding: 20px;
            background-color: #000;
            color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        #lblTitle {
            font-size: 24px;
            text-align: center;
            margin-bottom: 20px;
        }

        .checkbox-group {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            margin: 10px 0;
        }

        .checkbox-group label {
            font-size: 18px;
            color: #fff;
        }

        .checkbox-group input[type="checkbox"] {
            background-color: #000;
            border: 1px solid #fff;
        }

        #btnMostrarSelecao {
            padding: 10px 20px;
            background-color: #000;
            color: #fff;
            border: none;
            cursor: pointer;
        }

        #lblDiasSelecionados {
            font-size: 18px;
            text-align: center;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblTitle" runat="server" Text="Seleção de Dias da Semana" />
        <div class="checkbox-group">
            <asp:CheckBoxList ID="chkDiasSemana" runat="server" RepeatDirection="Vertical">
                <asp:ListItem Text="Domingo" Value="Domingo" />
                <asp:ListItem Text="Segunda-feira" Value="Segunda" />
                <asp:ListItem Text="Terça-feira" Value="Terca" />
                <asp:ListItem Text="Quarta-feira" Value="Quarta" />
                <asp:ListItem Text="Quinta-feira" Value="Quinta" />
                <asp:ListItem Text="Sexta-feira" Value="Sexta" />
                <asp:ListItem Text="Sábado" Value="Sabado" />
            </asp:CheckBoxList>
        </div>
        <div class="checkbox-group">
            <asp:Button ID="btnMostrarSelecao" runat="server" Text="Mostrar Seleção" OnClick="btnMostrarSelecao_Click" />
        </div>
        <div class="checkbox-group">
            <asp:Label ID="lblDiasSelecionados" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
