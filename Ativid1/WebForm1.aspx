﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Ativid1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CALCULADORA</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            text-align: center;
        }

        #form1 {
            margin: 0 auto;
            max-width: 400px;
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        #lblTitle {
            font-size: 24px;
            text-align: center;
            margin-bottom: 20px;
        }

        .input-group {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin: 10px 0;
        }

        .input-group select, .input-group input {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 16px;
        }

        #btnCalcular {
            padding: 10px 20px;
            background-color: #007BFF;
            color: #fff;
            border: none;
            cursor: pointer;
        }

        #lblResultado {
            font-size: 18px;
            text-align: center;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblTitle" runat="server" Text="CALCULADORA" />
        <div class="input-group">
            <asp:TextBox ID="txtNum1" runat="server" Placeholder="Número 1" />
        </div>
        <div class="input-group">
            <asp:DropDownList ID="ddlOperacao" runat="server">
                <asp:ListItem Text="Selecione a operação" Value="" />
                <asp:ListItem Text="Soma" Value="soma" />
                <asp:ListItem Text="Subtração" Value="subtracao" />
                <asp:ListItem Text="Multiplicação" Value="multiplicacao" />
                <asp:ListItem Text="Divisão" Value="divisao" />
            </asp:DropDownList>
        </div>
        <div class="input-group">
            <asp:TextBox ID="txtNum2" runat="server" Placeholder="Número 2" />
        </div>
        <div class="input-group">
            <asp:Button ID="btnCalcular" runat="server" Text="Calcular" OnClick="btnCalcular_Click" />
        </div>
        <div class="input-group">
            <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
