﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ativid1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            double num1, num2;
            if (double.TryParse(txtNum1.Text, out num1) && double.TryParse(txtNum2.Text, out num2))
            {
                string operacao = ddlOperacao.SelectedValue;
                double resultado = 0;

                switch (operacao)
                {
                    case "soma":
                        resultado = num1 + num2;
                        break;
                    case "subtracao":
                        resultado = num1 - num2;
                        break;
                    case "multiplicacao":
                        resultado = num1 * num2;
                        break;
                    case "divisao":
                        if (num2 != 0)
                        {
                            resultado = num1 / num2;
                        }
                        else
                        {
                            lblResultado.Text = "Divisão por zero não é permitida";
                            return;
                        }
                        break;
                    default:
                        lblResultado.Text = "Selecione uma operação valida";
                        return;
                }

                lblResultado.Text = "Resultado: " + resultado;

            }
            else
            {
                lblResultado.Text = "Insira numeros validos";
            }
        }


    }
}