﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ativid1
{
    public partial class _2Question : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnMostrarSelecao_Click(object sender, EventArgs e)
        {
            string diasSelecionados = "";

            foreach (ListItem item in chkDiasSemana.Items)
            {
                if (item.Selected)
                {
                    diasSelecionados += item.Text + ", ";
                }
            }

            if (!string.IsNullOrEmpty(diasSelecionados))
            {
               
                diasSelecionados = diasSelecionados.TrimEnd(' ', ',');
                lblDiasSelecionados.Text = "Dias Selecionados: " + diasSelecionados;
            }
            else
            {
                lblDiasSelecionados.Text = "Nenhum dia foi selecionado.";
            }
        }




    }
}